import React, { useState, useEffect } from "react";
import "./App.css";
import ContactCard from "./contactcard";
const App2 = () => {
  // const contacts = [
  //   // {
  //   //   avatarUrl: "https://via.placeholder.com/150",
  //   //   name: "Jenny Han",
  //   //   email: "Jenny.Han@notreal.com",
  //   //   age: 25,
  //   // },
  //   // {
  //   //   avatarUrl: "https://via.placeholder.com/150",
  //   //   name: "Penny Han",
  //   //   email: "Penny.Han@notreal.com",
  //   //   age: 35,
  //   // },
  // ];


  const [results, setResults] = useState([]);
  useEffect(() => {
    fetch("https://randomuser.me/api/?results=3")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setResults(data.results);
      });
  }, []);

  return results.map((result, index) => {
    return (
      <ContactCard
        key={index}
        avatarUrl={result.picture.large}
        name={`${result.name.first} ${result.name.last}`}
        email={result.email}
        age={result.dob.age}
      ></ContactCard>
    );
  });
};

export default App2;
