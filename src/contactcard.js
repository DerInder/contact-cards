import React, { useState } from "react";
import "./App.css";
const ContactCard = (props) => {
  const [showAge, setShowAge] = useState(props.showAge);
  const { avatarUrl, name, email, age } = props;
  const ToggleAge = () => {
    setShowAge(!showAge);
  };
  return (
    <div className="contact-card">
      <img src={avatarUrl} alt="profile" />
      <div className="user-details">
        <p>{name}</p>
        <p>{email}</p>
        <button onClick={ToggleAge}>Toggle Age</button>
        {showAge && <p>Age: {age}</p>}
      </div>
    </div>
  );
};

export default ContactCard;
