import React, { Component } from "react";
//import React, { Component } from 'react';
import ContactCard from "./contactcard";
class App extends Component {
  constructor() {
    super();
    this.state = {
      results: [],
    };
  }

  componentDidMount() {
    fetch("https://randomuser.me/api/?results=5")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        this.setState({ results: data.results });
      });
  }

  render() {
    let { results } = this.state;
    return results.map((result, index) => (
      <ContactCard
        key={index}
        avatarUrl={result.picture.large}
        name={`${result.name.first} ${result.name.last}`}
        email={result.email}
        age={result.dob.age}
        showAge ={true}
      />
    ));
  }
}

export default App;
